const DataManager = ModUtils.requireBase("managers/data");

// Basher
DataManager.addData("action", Object.assign({}, DataManager.getData("action", "base"), {
  key: "basher",
  getButtonSprites: function() {
    return {
      down: {
        atlas: "atlGUI",
        keys: ["Btn_Basher_1.png"]
      },
      up: {
        atlas: "atlGUI",
        keys: ["Btn_Basher_0.png"]
      }
    }
  }
}));

// Digger
DataManager.addData("action", Object.assign({}, DataManager.getData("action", "base"), {
  key: "digger",
  getButtonSprites: function() {
    return {
      down: {
        atlas: "atlGUI",
        keys: ["Btn_Digger_1.png"]
      },
      up: {
        atlas: "atlGUI",
        keys: ["Btn_Digger_0.png"]
      }
    }
  }
}));
