const DataManager = ModUtils.requireBase("managers/data");

DataManager.addType("action");
DataManager.addData("action", {
  key: "base"
});

require("./actions/classic");
