const Signal = require("../basic/signal");

function Input_Key() { this.initialize.apply(this, arguments); }

Input_Key.prototype.initialize = function(key) {
  this._key = key;
  this.down = false;
  this.pressed = false;
  this.released = false;
  this.onPress = new Signal();
  this.onRelease = new Signal();
  this.onRepeat = new Signal();
};

module.exports = Input_Key;
