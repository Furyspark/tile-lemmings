function ModUtils() {};

ModUtils.requireBase = function(relUrl) {
  return require(Core._dirs.electronRoot + "/src/" + relUrl);
};

module.exports = ModUtils;
const Core = require("./core");
