function DataManager() {};

DataManager._data = {};

DataManager.addType = function(key) {
  this._data[key] = {};
};

DataManager.addData = function(type, obj) {
  this._data[type][obj.key] = obj;
};

DataManager.getData = function(type, key) {
  return this._data[type][key];
};

module.exports = DataManager;
