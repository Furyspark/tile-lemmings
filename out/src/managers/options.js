const fs = require("fs");
const Signal = require("../basic/signal");
const Core = require("../core");

function Options() {}

Options.data          = null;
Options.onSave        = new Signal();
Options.onLoad        = new Signal();

Options.generate = function() {
  this.data = {};
  this.data.audio = {
    volume: {
      bgm: 0.9,
      snd: 0.9
    },
    toggleDuringPause: false
  };
  this.data.gameplay = {
    startWithGrid: false,
    maxRememberedFrames: 600,
    invertMouseScrolling: false
  };
};

Options.getSaveLocation = function() {
  return Core.getUserDataDir() + "/config.json";
};

Options.save = function() {
  let json = JSON.stringify(this.data);
  if(Core.usingElectron) {
    fs.writeFile(Options.getSaveLocation(), json, {}, function() {
      this.onSave.dispatch();
    }.bind(this));
  }
  else {
    localStorage.setItem("config", json);
    this.onSave.dispatch();
  }
};

Options.load = function() {
  this.generate();
  if(Core.usingElectron) {
    fs.readFile(Options.getSaveLocation(), {}, function(err, data) {
      if(!err) {
        data = JSON.parse(data);
        for(let a in data) {
          for(let b in data[a]) {
            this.data[a][b] = data[a][b];
          }
        }
      }
      this.onLoad.dispatch();
    }.bind(this));
  }
  else {
    let data = localStorage.getItem("config");
    if(data) this.data = Object.assign(this.data, JSON.parse(data));
    this.onLoad.dispatch();
  }
};

module.exports = Options;
