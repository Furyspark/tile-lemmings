function Game_Action() { this.initialize.apply(this, arguments); }

Game_Action.prototype.initialize = function(key, owner, vars) {
  this._key = key;
  this._owner = owner;
  this._vars = {};

  this.initVars(vars);
};

Game_Action.prototype.initVars = function(vars) {
  let source = this.getSource();

  for(let a in source.vars) {
    this._vars[a] = source.vars[a];
  }
  if(vars != null) {
    for(let a in vars) {
      this._vars[a] = vars[a];
    }
  }
};

Game_Action.prototype.getSource = function() {
  return DataManager.getData("action", this._key);
};

Game_Action.prototype.getOwner = function() {
  return this._owner;
};

Game_Action.prototype.runScript = function(name) {
  let args = [];
  for(let a = 1;a < arguments.length;a++) {
    args.push(arguments[a]);
  }

  let source = this.getSource();
  if(typeof source[name] === "function") {
    source[name].apply(this, [this, source, this.getOwner()].concat(args));
  }
};

module.exports = Game_Action;
const DataManager = ModUtils.requireBase("managers/data");
