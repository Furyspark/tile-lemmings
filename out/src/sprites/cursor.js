const Sprite_Base = require("./base");

function Sprite_Cursor() { this.initialize.apply(this, arguments); }

Sprite_Cursor.prototype = Object.create(Sprite_Base.prototype);
Sprite_Cursor.prototype.constructor = Sprite_Cursor;

Sprite_Cursor.prototype.initialize = function() {
  Sprite_Base.prototype.initialize.call(this);
  var anim = this.addAnimation("idle");
  anim.addFrame("atlMisc", "sprCursor_Idle.png");
  var anim = this.addAnimation("over");
  anim.addFrame("atlMisc", "sprCursor_Open.png");
  this.visible = false;
  this.scale.set(2);
  this.anchor.set(0.5);
  this.z = -2100;
};

module.exports = Sprite_Cursor;
