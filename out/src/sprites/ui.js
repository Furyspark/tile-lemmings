const Sprite_Base = require("./base");

function Sprite_UI() { this.initialize.apply(this, arguments); }

Sprite_UI.prototype = Object.create(Sprite_Base.prototype);
Sprite_UI.prototype.constructor = Sprite_UI;

Sprite_UI.prototype.initialize = function() {
  Sprite_Base.prototype.initialize.call(this);
  this.z = -100;
};

module.exports = Sprite_UI;
