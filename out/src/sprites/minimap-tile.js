const Sprite_Base = require("./base");

function Sprite_MinimapTile() { this.initialize.apply(this, arguments); }

Sprite_MinimapTile.prototype = Object.create(Sprite_Base.prototype);
Sprite_MinimapTile.prototype.constructor = Sprite_MinimapTile;

Sprite_MinimapTile.prototype.initialize = function() {
  Sprite_Base.prototype.initialize.call(this);
  this.addAnimationExt("atlMinimap", "ground", 1, "tile.png");
  this.addAnimationExt("atlMinimap", "steel", 1, "steel.png");
  this.addAnimationExt("atlMinimap", "liquid", 1, "water.png");
  this.playAnimation("water");
};

module.exports = Sprite_MinimapTile;
