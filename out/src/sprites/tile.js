const Sprite_Base = require("./base");

function Sprite_Tile() { this.initialize.apply(this, arguments); }

Sprite_Tile.prototype = Object.create(Sprite_Base.prototype);
Sprite_Tile.prototype.constructor = Sprite_Tile;

Sprite_Tile.prototype.initialize = function(texture) {
  Sprite_Base.prototype.initialize.call(this, texture);
  this.z = 0;
  this.animSpeed = 0.25;
};

Sprite_Tile.prototype.addAnimationFrame = function(animKey, tileset, index) {
  if(!this.animations[animKey]) this.addAnimation(animKey);
  let anim = this.getAnimation(animKey);
  let tex = tileset.getTileTexture(index);
  anim.frames.push(tex);
};

Sprite_Tile.prototype.update = function() {
  Sprite_Base.prototype.update.call(this);
  let arr = this.children.slice();
  for(let a = 0;a < arr.length;a++) {
    if(arr[a].update) arr[a].update();
  }
};

module.exports = Sprite_Tile;
