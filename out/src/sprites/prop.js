const Sprite_Base = require("./base");

function Sprite_Prop() { this.initialize.apply(this, arguments); }

Sprite_Prop.prototype = Object.create(Sprite_Base.prototype);
Sprite_Prop.prototype.constructor = Sprite_Prop;

Sprite_Prop.prototype.initialize = function() {
  Sprite_Base.prototype.initialize.call(this);
  this.anchor.set(0.5);
  this.z = 50;
  this.animSpeed = 1 / 2;
};

module.exports = Sprite_Prop;
