require("./lib");

global.$gameMap = null;
global.$gameTemp = {};


function Core() {}

Core._dataObjects = [
  { name: "$dataProps", key: "dataProps", src: "assets/data/props.yaml" },
  { name: "$dataActions", key: "dataActions", src: "assets/data/actions.yaml" }
];

Core.tileset = {};

Object.defineProperties(Core, {
  hRes: {
    get: function() { return Core.resolution.x / Core.renderer.width; }
  },
  vRes: {
    get: function() { return Core.resolution.y / Core.renderer.height; }
  }
});

Core.preStart = function() {
  this.config = null;
  this.initElectron();
  this.ipcRenderer.send("core", ["PRESTART"]);
};

Core.start = function() {
  this.initMembers();
  this.initPixi();
  this.fitToWindow();
  this.startDataObjects();
  this.initExternalLibs();
  this.initWindow();
  Input.initialize();

  let Scene_Boot = require("./scenes/boot");
  Loader.onComplete.addOnce(function() {
    SceneManager.push(new Scene_Boot());
    this.render();
  }, this);
};

Core.initMembers = function() {
  this.lastTime = new Date;
  this.fps = 0;
  this.frameRate = 30;
  this.debugMode = false;
  // Full screen
  this.isFullscreen = false;
  let func = function(e) {
  };
  if(document.onfullscreenchange != null) {
    document.addEventListener("fullscreenchange", this._onFullscreenChange.bind(this));
  }
  else if(document.onwebkitfullscreenchange != null) {
    document.addEventListener("webkitfullscreenchange", this._onFullscreenChange.bind(this));
  }
  else if(document.onmozfullscreenchange != null) {
    document.addEventListener("mozfullscreenchange", this._onFullscreenChange.bind(this));
  }
  // Resolution
  this.resolution = new Point(1280, 720);
  this.scale = 1;
  this.aspectRatio = this.resolution.x / this.resolution.y;
  // View
  this.rendererLeft = 0;
  this.rendererTop = 0;
};

Core._onFullscreenChange = function(e) {
  this.isFullscreen = !this.isFullscreen;
};

Core.initElectron = function() {
  this.usingElectron = false;
  if(typeof require === "function") {
    this.usingElectron = true;
    this.ipcRenderer = require("electron").ipcRenderer;
    this.fs = require("fs");
    this.initElectronProperties();
  }
};

Core.initElectronProperties = function() {
  // Send: Window resize
  window.addEventListener("resize", Core.onResize);
  this.ipcRenderer.on("core", function(ev, args) {
    let cmd = args.splice(0, 1)[0];
    switch(cmd.toUpperCase()) {
        // Listen: Start
      case "START":
        Core.initMainVariables(args[0]);
        Core.config = args[1];
        Core.start();
        break;
        // Listen: Debug mode
      case "DEBUG":
        Core.debugMode = true;
        break;
    }
  });
};

Core.initMainVariables = function(args) {
  this._dirs = args.dirs;
};

Core.initWindow = function() {
  // Maximize window
  if(this.config.window.maximized) {
    this.ipcRenderer.send("window", ["maximize"]);
  }
  // Set full screen
  if(this.config.window.fullscreen) {
    this.setFullscreen(true);
  }
};

Core.onResize = function(e) {
  Core.fitToWindow();
};

Core.initExternalLibs = function() {
  //createjs.Ticker.framerate = this.frameRate;
};

Core.initPixi = function() {
  if(PIXI.utils.isWebGLSupported()) {
    this.renderer = new PIXI.WebGLRenderer({ width: this.resolution.x, height: this.resolution.y });
  } else {
    this.renderer = new PIXI.CanvasRenderer({ width: this.resolution.x, height: this.resolution.y });
  }
  PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;
  document.body.appendChild(this.renderer.view);
};

Core.render = function() {
  // Update FPS
  let nowTime = new Date;
  if(nowTime.getSeconds() !== this.lastTime.getSeconds()) {
    this.fps = Math.floor(1000 / (nowTime - this.lastTime));
  }
  let dt = 100 * ((nowTime - this.lastTime) / 1000);
  this.lastTime = nowTime;
  // Set new timeout
  // requestAnimationFrame(this.render.bind(this));
  setTimeout(this.render.bind(this), Math.max(1, Math.floor(1000 / this.frameRate)));
  // Update scene
  Input.update();
  SceneManager.update(dt);
  Input._refreshButtonStates();
  SceneManager.render(dt);
};

Core.startDataObjects = function() {
  for(var a = 0;a < this._dataObjects.length;a++) {
    let dObj = this._dataObjects[a];
    let obj = Loader.loadYAML(dObj.key, dObj.src);
    obj.onComplete.addOnce(function(dataObject) {
      window[dataObject.name] = Cache.getJSON(dataObject.key);
    }, this, [dObj], 20);
  }
};

Core.fitToWindow = function() {
  let ww = window.innerWidth;
  let wh = window.innerHeight;
  let nw = ww;
  let nh = wh;
  if(ww / wh >= Core.aspectRatio) {
    nw = Math.floor(nh * Core.aspectRatio);
    Core.setScale(nh / Core.resolution.y);
  } else {
    nh = Math.floor(nw / Core.aspectRatio);
    Core.setScale(nw / Core.resolution.x);
  }
  Core.renderer.view.style.position = "absolute";
  Core.renderer.resize(nw, nh);
  Core.rendererLeft = Math.floor(ww / 2 - nw / 2);
  Core.rendererTop = Math.floor(wh / 2 - nh / 2);
  Core.renderer.view.style.left = Core.rendererLeft.toString() + "px";
  Core.renderer.view.style.top = Core.rendererTop.toString() + "px";
};

Core.setScale = function(value) {
  if(this.scale === value) return;
  this.scale = value;
  for(let a = 0;a < SceneManager._stack.length;a++) {
    let scene = SceneManager._stack[a];
    scene.stage.scale.set(this.scale);
  }
};

Core.resizeWindow = function(w, h) {
  if(Core.usingElectron) {
    let diffW = window.outerWidth - window.innerWidth;
    let diffH = window.outerHeight - window.innerHeight;
    Core.ipcRenderer.send("window", ["resize", w + diffW, h + diffH]);
  }
};

Core.centerWindow = function() {
  if(Core.usingElectron) {
    Core.ipcRenderer.send("window", ["center"]);
  }
};

Core.setFullscreen = function(state) {
  this.ipcRenderer.send("window", ["fullscreen", state]);
  this.isFullscreen = state;
};

Core.getFullscreen = function() {
  return this.isFullscreen;
};

Core.getUserDataDir = function() {
  return this._dirs._userData;
};

Core.getAssetsDir = function() {
  return this._dirs.electronRoot + "/assets";
};

module.exports = Core;
const Cache = require("./loader/cache");
const Point = require("./basic/point");
const Input = require("./input/input");
const SceneManager = require("./managers/scene");
const Loader = require("./loader/loader");
const PIXI = require("pixi.js");

global.ModUtils = require("./modutils");
