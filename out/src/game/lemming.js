const Game_Base = require("./base");

function Game_Lemming() { this.initialize.apply(this, arguments); }

Game_Lemming.prototype = Object.create(Game_Base.prototype);
Game_Lemming.prototype.constructor = Game_Lemming;

Game_Lemming.prototype.initialize = function() {
  Game_Base.prototype.initialize.call(this);
  this.sprite = new Sprite_Lemming();
  this.position = new Point();
  this.velocity = new Point();
  this.dir = new Point(1, 0);
  this.rotation = 0;
  this.actions = [];
  this.fallDistance = 0;
};

Game_Lemming.prototype.spawn = function(x, y) {
  Game_Base.prototype.spawn.call(this, x, y);
};

module.exports = Game_Lemming;
const Point = require("../basic/point");
const Rect = require("../basic/rect");
const Sprite_Lemming = require("../sprites/lemming");
