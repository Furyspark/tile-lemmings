const Scene_MenuBase = require("./menubase");
const Point = require("../basic/point");

function Scene_WorldMap() { this.initialize.apply(this, arguments); }

Scene_WorldMap.prototype = Object.create(Scene_MenuBase.prototype);
Scene_WorldMap.prototype.constructor = Scene_WorldMap;

Scene_WorldMap.CONTENT_OFFSET     = new Point(112, 96);
Scene_WorldMap.CONTENT_BUTTONSIZE = new Point(128, 128);
Scene_WorldMap.CONTENT_DATASIZE   = new Point(8, 4);
Scene_WorldMap.CONTENT_PADDING    = 16;


Scene_WorldMap.prototype.initialize = function() {
  Scene_MenuBase.prototype.initialize.call(this);
  this.current = "root";
  this.clearData();
  this.volatileGraphics = [];

  if(Cache.hasJSON("world")) Cache.removeJSON("world");
  let obj = Loader.loadYAML("world", "assets/data/world.yaml");
  obj.onComplete.addOnce(this.start, this);
};

Scene_WorldMap.prototype.start = function() {
  AudioManager.playBgm("bgmTitle");
  $gameWorld = Cache.getJSON("world");
  this.generateContent(this.current);
  this.fadeIn();
};

Scene_WorldMap.prototype.continue = function() {
  Scene_MenuBase.prototype.continue.call(this);
  $gameMap = null;
  AudioManager.playBgm("bgmTitle");
  this.generateContent(this.current);
  this.fadeIn();
};

Scene_WorldMap.prototype.clearData = function() {
  this.data = [];
  this.data.width = Scene_WorldMap.CONTENT_DATASIZE.x;
  this.data.height = Scene_WorldMap.CONTENT_DATASIZE.y;
  while(this.data.length < this.data.width * this.data.height) {
    this.data.push(null);
  }
  // Function: Replace
  this.data.replace = function(x, y, obj) {
    let index = (y * this.width) + x;
    return this.splice(index, 1, obj)[0];
  }
};

Scene_WorldMap.prototype.getDir = function(key) {
  return $gameWorld[key];
};

Scene_WorldMap.prototype.generateContent = function(key) {
  // Remove old content
  this.clear();

  let dir = this.getDir(key);
  // Create back button
  let elem = this.createBackButton();
  elem.onClick.add(this.fadeOut, this, [function() {
    this.goBack();
  }.bind(this)]);
  // Create content
  for(let a = 0;a < dir.contents.length;a++) {
    let content = dir.contents[a];
    this.createWorldButton(content);
  }
  this.parseWorldButtonRequirements();
};

Scene_WorldMap.prototype.createWorldButton = function(src) {
  // Determine position
  let pos = new Point(
    Scene_WorldMap.CONTENT_OFFSET.x + ((Scene_WorldMap.CONTENT_BUTTONSIZE.x + Scene_WorldMap.CONTENT_PADDING) * src.position.x),
    Scene_WorldMap.CONTENT_OFFSET.y + ((Scene_WorldMap.CONTENT_BUTTONSIZE.y + Scene_WorldMap.CONTENT_PADDING) * src.position.y)
  );
  // Create button
  let btn = new UI_WorldButton(pos, UI_WorldButton["TYPE_" + src.type.toUpperCase()], [src.icon]);
  btn.key = src.key;
  // Add event
  btn.onClick.add(this.fadeOut, this, [function(type, key) {
    this.enterWorld(btn, type, key);
  }.bind(this, src.type, src.key)]);
  // Add to UI list
  this.addUI(btn);
  // Add to data list
  this.data.replace(src.position.x, src.position.y, btn);
  return btn;
};

Scene_WorldMap.prototype.parseWorldButtonRequirements = function() {
  let dir = this.getDir(this.current);
  for(let a = 0;a < dir.contents.length;a++) {
    let src = dir.contents[a];
    let btn = this.getWorldButton(src.key);
    let meetsRequirements = true;
    // Determine requirements
    if(btn && src && src.require) {
      for(let b = 0;b < src.require.length;b++) {
        let requirement = src.require[b];
        // Map completion
        if(requirement.type === "map-completion") {
          let result = this.parseWorldButtonRequirement_MapCompletion(btn, requirement);
          if(meetsRequirements === true) meetsRequirements = result;
        }
      }
    }
    // Set map completion
    if(SaveManager.getMapCompletion(this.current, src.key)) {
      btn.setCompletion(UI_WorldButton.COMPLETE_COMPLETE);
    }
    else if(meetsRequirements) btn.setCompletion(UI_WorldButton.COMPLETE_INCOMPLETE);
    else btn.setCompletion(UI_WorldButton.COMPLETE_LOCKED);
    btn.checkCompletion();
  }
};

Scene_WorldMap.prototype.parseWorldButtonRequirement_MapCompletion = function(btn, requirement) {
  if(requirement.dir === this.current) {
    let btnTo = this.getWorldButton(requirement.key);
    let arrow = this.createArrow(btn.centerPos, btnTo.centerPos, 0x00ffff, 0x00aaaa);
  }
  if(SaveManager.getMapCompletion(requirement.dir, requirement.key)) return true;
  return false;
};

Scene_WorldMap.prototype.createArrow = function(fromPos, toPos, lineColor, arrowColor) {
  let gfx = new PIXI.Graphics();
  let len = fromPos.distanceTo(toPos);
  let arrowHeadSize = 24;
  let arrowHeadWidth = 36;
  gfx.beginFill(lineColor)
    .drawRect(0, -3, len - arrowHeadSize, 6)
    .endFill()
    .beginFill(arrowColor)
    .drawPolygon([
      new Point((len - arrowHeadSize), -(arrowHeadWidth / 2)),
      new Point((len - arrowHeadSize), (arrowHeadWidth / 2)),
      new Point(len, 0)
    ])
    .endFill();
  gfx.rotation = fromPos.rotationTo(toPos);
  gfx.position.set(fromPos.x, fromPos.y);
  gfx.z = 10;
  this.volatileGraphics.push(gfx);
  this.stage.addChild(gfx);
  return gfx;
};

Scene_WorldMap.prototype.getWorldButton = function(key) {
  for(let a = 0;a < this.data.length;a++) {
    let obj = this.data[a];
    if(obj && obj.key === key) return obj;
  }
  return null;
};

Scene_WorldMap.prototype.createBackButton = function() {
  let elem = new UI_MenuButton(new Point(40, 40), "Back");
  this.addUI(elem);
  return elem;
};

Scene_WorldMap.prototype.clear = function() {
  this.clearData();
  while(this.ui.length > 0) {
    let elem = this.ui.pop();
    if(elem.sprite) this.stage.removeChild(elem.sprite);
  }
  while(this.volatileGraphics.length > 0) {
    let elem = this.volatileGraphics.pop();
    elem.destroy();
  }
};

Scene_WorldMap.prototype.goBack = function() {
  let dir = this.getDir(this.current);
  if(dir.previous === null) {
    SceneManager.pop();
  }
  else {
    this.current = dir.previous;
    this.generateContent(this.current);
    this.fadeIn();
  }
};

Scene_WorldMap.prototype.enterWorld = function(btn, type, key) {
  if(type === "world") {
    this.current = key;
    this.generateContent(this.current);
    this.fadeIn();
  }
  else if(type === "map") {
    $gameTemp.currentMap = { world: this.current, key: key };
    SceneManager.push(new Scene_PreGame("assets/levels/" + this.current + "/" + key));
  }
};

module.exports = Scene_WorldMap;
const Scene_PreGame = require("./pregame");
const AudioManager = require("../managers/audio");
const SaveManager = require("../managers/save");
const SceneManager = require("../managers/scene");
const Loader = require("../loader/loader");
const Cache = require("../loader/cache");
const UI_WorldButton = require("../ui/world-button");
const UI_MenuButton = require("../ui/menu-button");
