const Scene_Base = require("./base");

function Scene_Boot() { this.initialize.apply(this, arguments); }

Scene_Boot.prototype = Object.create(Scene_Base.prototype);
Scene_Boot.prototype.constructor = Scene_Boot;

Scene_Boot.prototype.initialize = function() {
  Scene_Base.prototype.initialize.call(this);
  let obj = Loader.loadJSON("assetList", "assets/asset-list.json");
  obj.onComplete.addOnce(this.loadAssets, this);
};

Scene_Boot.prototype.loadAssets = function() {
  this._loadStage = { assets: false, options: false, save: false };

  Loader.onComplete.addOnce(function() {
    this._loadStage.assets = true;
    this._checkLoadCompletion();
  }, this);
  let assetList = Cache.getJSON("assetList");
  // Load Images
  for(let a = 0;a < assetList.images.length;a++) {
    let asset = assetList.images[a];
    Loader.loadImage(asset.key, asset.src);
  }
  // Load Texture Atlases
  for(let a = 0;a < assetList["texture-atlases"].length;a++) {
    let asset = assetList["texture-atlases"][a];
    Loader.loadTextureAtlas(asset.key, asset.src);
  }
  // Load audio
  for(let a = 0;a < assetList.audio.length;a++) {
    let asset = assetList.audio[a];
    Loader.loadAudio(asset.key, asset.src);
  }
  // Load data
  for(let a = 0;a < assetList.data.length;a++) {
    let asset = assetList.data[a];
    this.requireAsset("data/" + asset);
  }
  // Load options
  Options.onLoad.addOnce(function() {
    this._loadStage.options = true;
    this._checkLoadCompletion();
  }, this);
  Options.load();
  // Load save game
  SaveManager.onLoad.addOnce(function() {
    this._loadStage.save = true;
    this._checkLoadCompletion();
  }, this);
  SaveManager.load();
};

Scene_Boot.prototype.requireAsset = function(relUrl) {
  require(Core.getAssetsDir() + "/" + relUrl);
};

Scene_Boot.prototype._checkLoadCompletion = function() {
  for(let a in this._loadStage) {
    if(this._loadStage[a] === false) return;
  }
  this.start();
};

Scene_Boot.prototype.start = function() {
  Core.tileset.generic = new Game_Tileset();
  Core.tileset.generic.texture = Cache.getImage("tsGeneric");
  SceneManager.push(new Scene_MainMenu());
};

module.exports = Scene_Boot;
const Core = require("../core");
const Loader = require("../loader/loader");
const Cache = require("../loader/cache");
const Options = require("../managers/options");
const SaveManager = require("../managers/save");
const SceneManager = require("../managers/scene");
const Scene_MainMenu = require("./mainmenu");
const Game_Tileset = require("../map/tileset");
