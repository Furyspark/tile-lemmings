const UI_Base = require("./base");
const Signal = require("../basic/signal");
const Text = require("../basic/text");

function UI_Button() { this.initialize.apply(this, arguments); }

UI_Button.prototype = Object.create(UI_Base.prototype);
UI_Button.prototype.constructor = UI_Button;

UI_Button.prototype.initialize = function(x, y, key) {
  UI_Base.prototype.initialize.call(this, x, y, key);
  this.actOnPress = true;
  this.label = new Text("", {
    fill: "white",
    stroke: "black",
    strokeThickness: 2,
    fontSize: 14
  });
  this.label.anchor.x = 0.5;
  this.sprite.addChild(this.label);
  this.onClick = new Signal();
  this.lastClickTime = 0;
  this.z = -10;
};

UI_Button.prototype.click = function() {
  if(this.actOnPress) this.onClick.dispatch();

  let d = new Date();
  this.lastClickTime = d.getTime();
};

UI_Button.prototype.unclick = function() {
  if(!this.actOnPress) this.onClick.dispatch();
};

UI_Button.prototype.refresh = function() {
  if(this.label) {
    this.label.x = (this.sprite.width / this.sprite.scale.y) / 2;
    this.label.y = 0;
  }
};

module.exports = UI_Button;
